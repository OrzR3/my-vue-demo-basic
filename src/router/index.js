// 引入配置路由的相关信息
import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

const routes = [
  {
    path: '/halo',
    component: () => import('../components/HaloWord.vue'),
  },
];

// 创建路由对象
const router = new VueRouter({
  routes,
});

// 将 router 对象导出
export default router;
