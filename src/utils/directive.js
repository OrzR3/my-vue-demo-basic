
import Vue from 'vue'
// 注册一个全局自定义指令 `v-focus`
Vue.directive('focus', {
  // 当被绑定的元素插入到 DOM 中时……
  inserted: function (el) {
    // 聚焦元素
    el.focus()
  }
})

Vue.directive('click', {
  inserted(el, binding) {
      el.addEventListener('click', () => {
          if (!el.disabled) {
              el.disabled = true;
              setTimeout(() => {
                  el.disabled = false;
              }, binding.value || 1000)
          }
      })
  }
})

Vue.directive("color",{
  // bind：只调用一次，指令第一次绑定到元素时调用。在这里可以进行一次性的初始化设置
  bind:function(el,binding){
      console.log(binding.name)
      //有值就用，没值默认颜色
      el.style.color=binding.value || "#F56C6C" 
  },
  // inserted：被绑定元素插入父节点时调用
  inserted:function(el){
    console.log(el)
  },
  // update：所在组件的 VNode 更新时调用
  update:function(el){
    console.log(el)
  }
})