import Vue from "vue";
import App from "./App.vue";
import "./utils/directive";

// 引入路由对象
import router from "./router";

import VueClipBoard from "vue-clipboard2";
Vue.use(VueClipBoard);

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

Vue.prototype.$EventBus = new Vue();
Vue.config.productionTip = false;
// eslint-disable-next-line
import store from "./store";
new Vue({
  store,
  // 挂载路由对象
  router,
  render: (h) => h(App),
}).$mount("#app");
